import { Sprite, Container } from "pixi.js";

export class Bunny extends Container {
    private body: Sprite;
    constructor() {
        super();
        this.body = Sprite.from("assets/bunny.png");
        this.addChild(this.body);

        this.body.anchor.set(0.5);
        this.x = 300;
        this.y = 300;
    }
}