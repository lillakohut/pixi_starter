import {Sprite, Application} from "pixi.js";
import { Bunny } from "./bunny";

const app = new Application({backgroundColor: 0x22AA00});
document.body.appendChild(app.view);

const bunny = new Bunny();

app.stage.addChild(bunny);

let velocity = 5;
app.ticker.add(() => {
    
    // childBunnies.forEach(b => b.rotation += 0.1);
    bunny.x += velocity;
    if (bunny.x + bunny.width * 0.5 > app.view.width) {
        velocity *= -1;
        bunny.x = app.view.width - bunny.width * 0.5;
    } else if (bunny.x < bunny.width * 0.5) {
        velocity *= -1;
        bunny.x = bunny.width * 0.5;
    }
});

const childBunnies = [];
for (let i = 0; i < 100; i++) {
    const childBunny = Sprite.from("assets/bunny.png");
    childBunny.scale.x = 0.1;
    childBunny.scale.y = 0.1;
    childBunny.x = Math.random() * bunny.width - bunny.width * 0.5; 
    childBunny.y = Math.random() * bunny.height - bunny.height * 0.5; 
    childBunny.anchor.set(0.5);
bunny.addChild(childBunny);
childBunnies.push(childBunny);
}